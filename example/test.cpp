#include "msp430g2553.h"
#include "ST7920.h"
#include "Graphics.h"

ST7920 ST7920;

void drawIntro()
{
    ST7920.drawImage(LOGO, 1, 8, 32, 48);

    ST7920.printString("HACETTEPE", 3, 8);
    ST7920.printString("UNIVERSITY", 3, 18);

    ST7920.printString("ELE417", 3, 28);

    ST7920.printString("TicTacToe", 3, 48);

}

void main()
{

    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    ST7920.init();

    drawIntro();

    while(1);
}
