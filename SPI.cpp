/*
 * SPI.cpp
 *
 *  Created on: Dec 23, 2020
 *
 */

#include <SPI.h>

SPI::SPI()
{
    // TODO Auto-generated constructor stub

}

SPI::~SPI()
{
    // TODO Auto-generated destructor stub
}

void SPI::init()
{
    UCB0CTL1 = UCSWRST;

    P2DIR  |= CS;
    P2OUT  |= CS;
    P1SEL  |= SIMO + SCLK;
    P1SEL2 |= SIMO + SCLK;

    // 3-pin, 8-bit SPI master
    UCB0CTL0 |= UCCKPH + UCMSB + UCMST + UCSYNC;
    UCB0CTL1 |= UCSSEL_2;   // SMCLK

    UCB0CTL1 &= ~UCSWRST;

    setCS(false);
}

void SPI::setCS(bool high)
{
    if (high)   P2OUT |=  CS;
    else        P2OUT &= ~CS;
}

void SPI::write(unsigned char data)
{
    while (!(IFG2 & UCB0TXIFG));

    UCB0TXBUF = data;

    while (!(IFG2 & UCB0TXIFG));
}
