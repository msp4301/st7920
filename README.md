# ST7920

## Wiring

LCD should be connected to the MSP430G2 Launchpad as shown below:

![](https://gitlab.com/msp4301/st7920/-/raw/master/Schematics/ST7920_Wiring.jpg)
